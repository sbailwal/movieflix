# Movieflix

This is an Angular 6 application. This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3. This has now been updated to 1.5.4

Objective of this project is to learn to add PWA capabilities to an app.

# Getting Started

*It is assumed that you have [node and npm](https://nodejs.org/en/download/) installed*. Check using `node -v` and `npm -v`. If needed, update to the latest.


Simply follow below steps to get started locally. 

1. Clone the app, `git clone https://sbailwal@bitbucket.org/sbailwal/movieflix.git`. This will create a new folder (movieflix) and download all the files in it.
2. Go to the project folder, `cd movieflix`
3. Install: Install required packages/dependencies, `npm install`
4. Build: Build the project, `npm run build --prod --aot`. After build is successful, it should create a **dist** folder
5. Run: Go to **dist** folder, `cd dist`, and run the web server, `live-server`. 
* *I perfer to use [live-server](https://www.npmjs.com/package/live-server) as it's light-weight and provides live reload capabilities.*

Console will display something like **Serving "/Users/<yourusername>/.../movieflix/dist" at http://127.0.0.1:8080**. At this time app should be automatically launched in the browser.

Web application is up and ready to be customized!

# Build and Run the App 

Below are few of the parameters used to execute in different modes.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
